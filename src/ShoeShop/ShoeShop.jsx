import React, { Component } from "react";
import ListItem from "../ShoeShop/Component/ListItem";
import TableCart from "../ShoeShop/Component/TableCart";

export default class ShoeShop extends Component {
  render() {
    return (
      <div>
        <TableCart />
        <ListItem />
      </div>
    );
  }
}
