import { ADD_TO_CART, GIAM_SO_LUONG, TANG_SO_LUONG } from "../contant/contant";

export const handleAddToCart = (shoe) => {
  return {
    type: ADD_TO_CART,
    payload: shoe,
  };
};

export const tangSoLuong = (payload) => ({
  type: TANG_SO_LUONG,
  payload,
});
export const giamSoLuong = (payload) => ({
  type: GIAM_SO_LUONG,
  payload,
});
