import { combineReducers } from "redux";
import { ShoeShopReducer } from "./reducer/ShoeShopReducer";
export let rootReducer = combineReducers({ ShoeShopReducer });
