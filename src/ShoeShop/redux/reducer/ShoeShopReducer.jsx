import { data_shoes } from "../../dataShoes";
import { ADD_TO_CART, GIAM_SO_LUONG, TANG_SO_LUONG } from "../contant/contant";
const initialState = {
  dataShoes: data_shoes,
  cart: [],
};

export const ShoeShopReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TO_CART: {
      let newCart = [...state.cart];
      let index = newCart.findIndex((item) => {
        return item.id == action.payload.id;
      });
      if (index == -1) {
        let newItem = { ...action.payload, quality: 1 };
        newCart = [...newCart, newItem];
      } else {
        newCart[index].quality++;
      }

      state.cart = newCart;
      return { ...state };
    }
    case TANG_SO_LUONG: {
      let newCart = [...state.cart];
      let index = action.payload;
      newCart[index].quality++;
      state.cart = newCart;
      return { ...state };
    }
    case GIAM_SO_LUONG: {
      let newCart = [...state.cart];
      let index = action.payload;
      newCart[index].quality == 1
        ? newCart.splice(index, 1)
        : newCart[index].quality--;
      state.cart = newCart;
      return { ...state };
    }
    default:
      return state;
  }
};
