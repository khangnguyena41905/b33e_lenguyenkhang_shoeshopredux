import React, { Component } from "react";
import { connect } from "react-redux";
import { handleAddToCart } from "../redux/action/action.jsx";
class ItemShoes extends Component {
  renderItem = () => {
    return this.props.dataShoes.map((item, index) => {
      return (
        <div key={index} className="card col-4" style={{ width: "100%" }}>
          <img className="card-img-top" src={item.image} alt="Card image cap" />
          <div className="card-body">
            <h5 className="card-title">Card title</h5>
            <p className="card-text">
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
            </p>
            <button
              onClick={() => {
                this.props.handleAddToCart(item);
              }}
              className="btn btn-primary"
            >
              Add to cart{" "}
            </button>
          </div>
        </div>
      );
    });
  };
  render() {
    return <div className="row">{this.renderItem()}</div>;
  }
}
let mapStateToProps = (state) => {
  return {
    dataShoes: state.ShoeShopReducer.dataShoes,
  };
};
let mapDispathToProps = (dispath) => {
  return {
    handleAddToCart: (shoe) => {
      dispath(handleAddToCart(shoe));
    },
  };
};
export default connect(mapStateToProps, mapDispathToProps)(ItemShoes);
