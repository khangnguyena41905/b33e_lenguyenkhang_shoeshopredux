import React, { Component } from "react";
import { connect } from "react-redux";
import { tangSoLuong, giamSoLuong } from "../redux/action/action";
class TableCart extends Component {
  renderShoe = () => {
    return this.props.cart.map((item, index) => {
      return (
        <tr key={index}>
          <th scope="row">{item.id}</th>
          <td>{item.name}</td>
          <td>
            <img src={item.image} style={{ width: 60 }} alt="" />
          </td>
          <td>{item.price * item.quality}</td>
          <td>
            <button
              onClick={() => {
                this.props.giamSoLuong(index);
              }}
              type="button"
              class="btn btn-danger"
            >
              -
            </button>
            {item.quality}
            <button
              onClick={() => {
                this.props.tangSoLuong(index);
              }}
              type="button"
              class="btn btn-warning"
            >
              +
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th scope="col">ID</th>
              <th scope="col">Name</th>
              <th scope="col">Image</th>
              <th scope="col">Price</th>
              <th scope="col">Quality</th>
            </tr>
          </thead>
          <tbody>{this.renderShoe()}</tbody>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    cart: state.ShoeShopReducer.cart,
  };
};
let mapDispathToProps = (dispath) => {
  return {
    tangSoLuong: (index) => {
      dispath(tangSoLuong(index));
    },
    giamSoLuong: (index) => {
      dispath(giamSoLuong(index));
    },
  };
};
export default connect(mapStateToProps, mapDispathToProps)(TableCart);
